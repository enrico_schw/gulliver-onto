import javax.servlet.ServletContext

import _root_.akka.actor.{ActorSystem, Props}
import de.shig.gulliveronto.api.rest.individual.controller.LearningInstanceController
import de.shig.gulliveronto.api.rest.version.controller.VersionController
import de.shig.gulliveronto.app.cluster.LearningInstance
import org.scalatra._

class ScalatraBootstrap extends LifeCycle {

    val system = ActorSystem()
    val learningInstance = system.actorOf(Props[LearningInstance])

    override def init(context: ServletContext) {
        context.mount(new VersionController, "/version/*")
        context.mount(new LearningInstanceController(system, learningInstance), "/graph/*")
    }

    override def destroy(context: ServletContext) {
        system.shutdown()
    }
}
