package de.shig.gulliveronto.app.cluster

import akka.actor.Actor
import akka.event.Logging


class LearningInstance extends Actor {

    val log = Logging(context.system, this)

    override def preStart() {
        log.info("Start LearningInstance Actor")
    }

    def receive = {
        case "Do stuff and give me an answer" => sender ! "The answer is 42"
        case "Hey, you know what?" => log.info("Yeah I know... oh boy do I know")
        case _ => log.info("received unknown message")
    }
}
