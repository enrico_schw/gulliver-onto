package de.shig.gulliveronto.api

import org.json4s.{DefaultFormats, Formats}
import org.scalatra._

// JSON handling support from Scalatra

import org.scalatra.json._


trait GulliverontoStack extends ScalatraServlet with JacksonJsonSupport {

    // Sets up automatic case class to JSON output serialization, required by
    // the JValueResult trait.
    protected implicit val jsonFormats: Formats = DefaultFormats

    // Before every action runs, set the content type to be in JSON format.
    before() {
        contentType = formats("json")
    }
}
