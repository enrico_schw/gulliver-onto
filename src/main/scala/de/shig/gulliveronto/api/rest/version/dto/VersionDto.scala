package de.shig.gulliveronto.api.rest.version.dto

case class VersionDto(version: String)
