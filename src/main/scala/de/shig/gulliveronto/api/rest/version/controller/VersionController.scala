package de.shig.gulliveronto.api.rest.version.controller

import de.shig.gulliveronto.api.GulliverontoStack
import de.shig.gulliveronto.api.rest.version.factory.VersionFactory

class VersionController extends GulliverontoStack {

    get("/") {
        VersionFactory.getVersion()
    }

}
