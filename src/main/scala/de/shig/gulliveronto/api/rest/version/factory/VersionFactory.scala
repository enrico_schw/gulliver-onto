package de.shig.gulliveronto.api.rest.version.factory

import com.typesafe.config.ConfigFactory
import de.shig.gulliveronto.api.rest.version.dto.VersionDto


object VersionFactory {

    def getVersion(): VersionDto = {
        val conf = ConfigFactory.load()
        return new VersionDto(conf.getString("gulliver-onto.version"))
    }
}
