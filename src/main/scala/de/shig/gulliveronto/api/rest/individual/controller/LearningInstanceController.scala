package de.shig.gulliveronto.api.rest.individual.controller

import akka.actor.{ActorRef, ActorSystem}
import akka.pattern.ask
import akka.util.Timeout
import de.shig.gulliveronto.api.GulliverontoStack
import de.shig.gulliveronto.api.rest.individual.dto.LearningInstanceDto
import org.scalatra.{Accepted, FutureSupport}

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}


class LearningInstanceController(system: ActorSystem, learningInstance: ActorRef) extends GulliverontoStack with
FutureSupport {

    implicit val timeout = new Timeout(2 seconds)

    protected implicit def executor: ExecutionContext = system.dispatcher

    // You'll see the output from this in the browser.
    get("/ask") {
        val future: Future[String] = ask(learningInstance, "Do stuff and give me an answer").mapTo[String]

        val result = Await.result(future, timeout.duration).asInstanceOf[String]

        new LearningInstanceDto(result)
    }

    // You'll see the output from this in your terminal.
    get("/tell") {
        learningInstance ! "Hey, you know what?"
        Accepted()
    }

}



