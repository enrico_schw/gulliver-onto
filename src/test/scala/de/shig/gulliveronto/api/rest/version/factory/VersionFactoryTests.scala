package de.shig.gulliveronto.api.rest.version.factory

import com.typesafe.config.ConfigFactory
import de.shig.gulliveronto.api.rest.version.dto.VersionDto
import org.scalatest.FunSuite

class VersionFactoryTests extends FunSuite {

    test("The version should read from application.conf") {
        val conf = ConfigFactory.load()
        val versionExpected = conf.getString("gulliver-onto.version")
        def versionDto = VersionFactory.getVersion()
        assert(versionDto.version == versionExpected)
        assertResult(new VersionDto(versionExpected))(versionDto)
    }
}
