package de.shig.gulliveronto.api.rest.version.controller

import com.typesafe.config.ConfigFactory
import org.scalatest.FunSuiteLike
import org.scalatra.test.scalatest.ScalatraSuite

class VersionControllerTests extends ScalatraSuite with FunSuiteLike {

    addServlet(classOf[VersionController], "/*")

    test("GET / on VersionController") {
        val conf = ConfigFactory.load()
        val version = conf.getString("gulliver-onto.version")

        get("/") {
            status should equal(200)
            body should equal("{\"version\":\"" + version + "\"}")
        }
    }
}
