package de.shig.gulliveronto.api.rest.version.factory

import org.scalameter.api._

object VersionFactoryBenchmark extends PerformanceTest {
    /* configuration */

    lazy val executor = LocalExecutor(
        new Executor.Warmer.Default,
        Aggregator.min,
        new Measurer.Default)
    lazy val reporter = new LoggingReporter
    lazy val persistor = Persistor.None

    /* inputs */

    val sizes = Gen.range("size")(300000, 1500000, 300000)

    val ranges = for {
        size <- sizes
    } yield 0 until size

    /* tests */
    performance of "Range" in {
        measure method "map" in {
            using(ranges) in {
                r => VersionFactory.getVersion()
            }
        }
    }
}
