import com.mojolly.scalate.ScalatePlugin._
import org.scalatra.sbt._
import sbt.Keys._
import sbt._


object GulliverontoBuild extends Build {
    val Organization = "de.shig"
    val Name = "gulliver-onto"
    val Version = "0.1.0-SNAPSHOT"
    val ScalaVersion = "2.11.1"
    val ScalatraVersion = "2.3.0"

    lazy val project = Project(
        "gulliver-onto",
        file("."),
        settings = ScalatraPlugin.scalatraWithJRebel ++ scalateSettings ++ Seq(
            organization := Organization,
            name := Name,
            version := Version,
            scalaVersion := ScalaVersion,
            resolvers ++= Seq(
                Classpaths.typesafeReleases,
                "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/releases"
            ),
            unmanagedSourceDirectories in Test += baseDirectory.value / "src" / "benchmark" / "scala",
            libraryDependencies ++= Seq(
                "org.scalatra" %% "scalatra" % ScalatraVersion,
                "org.scalatra" %% "scalatra-scalate" % ScalatraVersion,
                "org.scalatra" %% "scalatra-scalatest" % ScalatraVersion % "test",
                "org.scalatra" %% "scalatra-json" % ScalatraVersion,
                "com.typesafe.akka" %% "akka-actor" % "2.3.4",
                "net.databinder.dispatch" %% "dispatch-core" % "0.11.1",
                "org.json4s" %% "json4s-jackson" % "3.2.9",
                "org.skife.com.typesafe.config" % "typesafe-config" % "0.3.0",
                "com.storm-enroute" %% "scalameter" % "0.6",
                "ch.qos.logback" % "logback-classic" % "1.1.2" % "runtime",
                "org.eclipse.jetty" % "jetty-webapp" % "9.1.5.v20140505" % "container",
                "org.eclipse.jetty" % "jetty-plus" % "9.1.5.v20140505" % "container",
                "javax.servlet" % "javax.servlet-api" % "3.1.0"
            ),
            testFrameworks += new TestFramework("org.scalameter.ScalaMeterFramework"),
            parallelExecution in Test := false
        )
    )
}
