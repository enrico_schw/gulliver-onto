#Gulliver onto


## Introduction

The Gulliver onto is recommendation service system for learning platforms. Those includes the learning of students with their teacher and without teachers. The system is build as grails plugin with a REST-API. 

 - [Setup](##setup)
 - [Run](##run)
 - [Test](##test)
 - [External dependencies](#external-dependencies)

## Setup

### Project setup

    git clone git@bitbucket.org:enrico_schw/gulliver-onto.git
    cd gulliver-onto/


## Run
If you haven't already done so, from your project root, you can run the project:
    
    $ ./sbt
    > container:start

The application starts on [http://localhost:8080](http://localhost:8080).

### Automatic code reloading

    $ ./sbt
    > container:start
    > ~ ;copy-resources;aux-compile

##Test

### Run all Test

    $ ./sbt
    > test

or run 

    $ ./sbt test

### Run only tests Test

## Run only Benchmark

   $ ???


## External dependencies
The Gulliver onto project mainly uses this tools for managing and building the application

* Scalatra (http://www.scalatra.org/) opyright (c) Alan Dipert <alan.dipert@gmail.com>. All rights reserved.
* Apache Jena (https://jena.apache.org/) License: Apache 2.0 License
* DocTest (https://github.com/devbliss/doctest) License: Apache 2.0 License
* Vagrant (https://github.com/mitchellh/vagrant/) License: MIT License
* VirtualBox (https://www.virtualbox.org/) License: MIT license

[&#8674; scroll up to index of content](#introduction)
